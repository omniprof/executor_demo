package com.kenfogel.executor_demo;

import com.kenfogel.executor_demo.task.EmailTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class puts up a simple stage and then uses an executor to schedule a
 * task that runs in its own thread to execute at a regular interval, sending an
 * email every 20 seconds.
 *
 * @author Ken Fogel
 * @version 1.0
 */
public class ExecutorAppFX extends Application {

    // slf4j log4j logger
    private final static Logger LOG = LoggerFactory.getLogger(ExecutorAppFX.class);

    private Stage primaryStage;
    private final EmailTask emailTask;
    private final ScheduledExecutorService executor;

    /**
     * Default constructor instantiates the task to be carried out and the
     * executor that manages the interval
     */
    public ExecutorAppFX() {
        emailTask = new EmailTask();

        // Multiple tasks can be scheduled, here we are scheduling just one
        executor = Executors.newScheduledThreadPool(1);
    }

    /**
     * The required start method. After creating the UI it starts the executor.
     * The executor runs in its own thread and at some interval will execute an
     * object that implements the Runnable interface.
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        initRootLayout();

        // The interval is between the start time of the task.
        // The task must complete its task before the next interval completes.
        // Runnable task, delay to start, interval between tasks, time units
        executor.scheduleWithFixedDelay(emailTask, 20, 20, TimeUnit.SECONDS);
    }

    /**
     * Build the simple layout
     */
    private void initRootLayout() {
        HBox root = new HBox(10);
        root.setPadding(new Insets(10));

        Label label = new Label("Threaded Timer");
        label.setFont(new Font("Arial", 30));

        // Here is an exit button that invokes Platform.exit(). If you do not
        // first do an executor.shutdown() then the Executor will keep running
        // even though the UI has been exited.
        Button btn = new Button("Exit");
        btn.setFont(new Font("Arial", 30));
        btn.setOnAction(event -> {
            executor.shutdown();
            Platform.exit();
        });

        root.getChildren().addAll(label, btn);

        // Event when the window is closed by an external event such as by
        // clicking on Window exit decoration (the X in the title bar).
        // A Platform.exit() is implied but the executor thread must be stopped.
        primaryStage.setOnCloseRequest(event -> {
            executor.shutdown();
            LOG.info("Window close icon pressed");
        });

        Scene scene = new Scene(root);

        primaryStage.setTitle("Timer");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
}
