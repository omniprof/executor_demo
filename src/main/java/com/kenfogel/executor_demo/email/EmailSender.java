package com.kenfogel.executor_demo.email;

import java.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jodd.mail.Email;
import jodd.mail.MailServer;
import jodd.mail.RFC2822AddressParser;
import jodd.mail.SendMailSession;
import jodd.mail.SmtpServer;

/**
 * This class sends a simple email for the purpose of the demo
 *
 * @author Ken
 * @version 1.0
 */
public class EmailSender {

    // Real programmers use logging
    private final static Logger LOG = LoggerFactory.getLogger(EmailSender.class);

    // These must be updated to your email accounts
    private final String smtpServerName = "smtp.gmail.com";
    private final String emailSend = "***********";
    private final String emailReceive = "***********";
    private final String emailSendPwd = "***********";

    /**
     * Standard send routine using Jodd. Jodd knows about GMail so no need to
     * include port information
     */
    public void sendEmail() {

        if (checkEmail(emailSend) && checkEmail(emailReceive)) {
            // Create am SMTP server object
            SmtpServer smtpServer = MailServer.create()
                    .ssl(true)
                    .host(smtpServerName)
                    .auth(emailSend, emailSendPwd)
                    .debugMode(true)
                    .buildSmtpMailServer();

            // Display Java Mail debug conversation with the server
            //smtpServer.debugMode(true);
            //
            // Using the fluent style of coding create a plain text message
            Email email = Email.create().from(emailSend)
                    .to(emailReceive)
                    .subject("Threaded Email").textMessage("This email was send at " + LocalDateTime.now());

            // Like a file we open the session, send the message and close the
            // session
            try ( // A session is the object responsible for communicating with the server
                    SendMailSession session = smtpServer.createSession()) {
                // Like a file we open the session, send the message and close the
                // session
                session.open();
                session.sendMail(email);
                LOG.info("Email sent");
            }
        } else {
            LOG.info("Unable to send email because either send or recieve addresses are invalid");
        }
    }

    /**
     * Use the RFC2822AddressParser to validate that the email string could be a
     * valid address
     *
     * @param address
     * @return true is OK, false if not
     */
    private boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }
}
