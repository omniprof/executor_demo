package com.kenfogel.executor_demo.task;

import com.kenfogel.executor_demo.email.EmailSender;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is a class that, by implementing the runnable interface, can be
 * scheduled by an Executor. Class variables that are final (immutable) are
 * thread safe because they may not be changed.
 *
 * @author Ken Fogel
 * @version 1.0
 */
public class EmailTask implements Runnable {

    // slf4j log4j logger
    private final static Logger LOG = LoggerFactory.getLogger(EmailTask.class);

    // As we will be using this object in a thread it must be thread safe.
    // This is accomplished by ensuring that all its class variables are final
    // as well.
    private final EmailSender emailSender;

    /**
     * Constructor creates an instance of the thread safe EmailSender
     */
    public EmailTask() {
        emailSender = new EmailSender();
    }

    /**
     * This method is called by the executor when the defined interval ends
     */
    @Override
    public void run() {

        // Platform.runLater() allows this thread to interact with the main
        // JavaFX thread and therefore interact with controls.
        Platform.runLater(() -> {
            // Uncomment after providing email credentials
            //emailSender.sendEmail();

            LOG.info("Sent an email");

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Sending An Email");
            alert.setHeaderText("Sending An Email.");
            alert.setContentText("An email has been sent.");
            alert.showAndWait();
        });
    }
}
